### Steps

- move to AppBundle
- move to constants: latest_comment_limit
- remove DI directory, import services.yml directly
- remove class parameters
- rename the Twig extension
- shorten the service names
- move services.yml to app/config
- move routes into app/config
- simplified homepage route name (and then all route names)
- PageController, use redirectToRoute and addFlash
- move *some* routing to annotations
- use ParamConverter in some cases
- add an isSubmitted() to that
- remove the layout.html.twig
- move some templates to app/Resources/views
- move CSS into the web/ dir

### Maybe?
- use bcrypt in security
- use @Security and access_control
- add some security?
- translations?


### TODO

+ 1) Add slides for all steps
- 2) Add expert activities for each step
+ 3) Add any additional new step we want to pad things out
+ 4) Create branches for each step
+ 5) Create a bit.ly with details about how to switch to each branch
+ 6) Create the step2 branch and update that slide
- 7) Make the repository public

