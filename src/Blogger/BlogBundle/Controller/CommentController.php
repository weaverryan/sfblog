<?php

namespace Blogger\BlogBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Blogger\BlogBundle\Entity\Comment;
use Blogger\BlogBundle\Form\CommentType;
use Symfony\Component\HttpFoundation\Request;

/**
 * Comment controller.
 */
class CommentController extends Controller
{
    public function _newFormAction($id)
    {
        $blog = $this->getBlog($id);

        $comment = new Comment();
        $comment->setBlog($blog);
        $form    = $this->createCommentForm($comment);

        return $this->render('BloggerBlogBundle:Comment:_form.html.twig', array(
            'comment' => $comment,
            'form'   => $form->createView()
        ));
    }

    public function newAction($id, Request $request)
    {
        $blog = $this->getBlog($id);

        $comment = new Comment();
        $comment->setBlog($blog);
        $form    = $this->createCommentForm($comment);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()
                       ->getManager();
            $em->persist($comment);
            $em->flush();

            return $this->redirect($this->generateUrl('BloggerBlogBundle_blog_show', array(
                    'id'    => $comment->getBlog()->getId(),
                )) . '#comment-' . $comment->getId()
            );
        }

        return $this->render('BloggerBlogBundle:Comment:new.html.twig', array(
            'comment' => $comment,
            'form'    => $form->createView()
        ));
    }

    protected function getBlog($blog_id)
    {
        $em = $this->getDoctrine()
                    ->getManager();

        $blog = $em->getRepository('BloggerBlogBundle:Blog')->find($blog_id);

        if (!$blog) {
            throw $this->createNotFoundException('Unable to find Blog post.');
        }

        return $blog;
    }

    private function createCommentForm(Comment $comment)
    {
        $form = $this->createForm(new CommentType(), $comment, array(
            'action' => $this->generateUrl('BloggerBlogBundle_comment_new', array(
                'id' => $comment->getBlog()->getId()
            ))
        ));
        $form->add('save', 'submit');

        return $form;
    }
}
